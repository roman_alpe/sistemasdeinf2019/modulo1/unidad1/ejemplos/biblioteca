﻿-- Ejemplo de creación de una base de datos de veterinarios
-- Tiene 3 tablas
DROP DATABASE IF EXISTS b20190605;
CREATE DATABASE b20190605;

-- Seleccionar la base de datos
USE b20190605;

/*
  creando la tabla clientes
*/

 CREATE TABLE ejemplar(
  cod_ejem int,
  titulo varchar(100),
  PRIMARY KEY(cod_ejem) -- creando la clave

);
CREATE TABLE socio(
  cod_socio int,
  nombre varchar(100),
  apell varchar(100),
  PRIMARY KEY(cod_socio) -- creando la clave
);

CREATE TABLE presta(
  cod_ejem int,
  cod_soc int,
  f_inic date,
  f_fin date,
  
  PRIMARY KEY(cod_ejem,cod_soc), -- creando la clave
  
  CONSTRAINT fksociopresta FOREIGN KEY(cod_soc)
  REFERENCES socio(cod_socio),
  CONSTRAINT fkpresta_ejemp FOREIGN KEY(cod_ejem)
  REFERENCES ejemplar(cod_ejem)

  );